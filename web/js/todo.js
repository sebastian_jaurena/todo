var todo = {
  
  templates: {
    notification: null,
    view: null,
    form: null
  },
  
  tasks: null,
  notifications: null,
  
  init: function(){
    
    this.tasks = $('#tasks');
    this.notifications = $('.notifications').first();
    
    this.templates.notification = _.template($('#notification-template').html());
    this.templates.view = _.template($('#task-view-template').html());
    this.templates.form = _.template($('#task-form-template').html());
    
    this.loadEvents();
    
    this.getTasks({ 
      orderby: this.getOrder()
    });
  },
  
  getTaskView: function(task){
    return this.templates.view(task);
  },
  
  getTaskForm: function(task){
    return this.templates.form(task);
  },
  
  addNotification: function(notificationType, text){
    
    var notification = $('<div></div>').append(this.templates.notification({
      type: notificationType,
      message: text
    })).hide();
    
    this.notifications.append(notification);
    
    notification.slideDown('slow').delay(5000).fadeOut('slow', function(){
      $(this).remove();
    });
  },
  
  getLoading: function(){
    return $('#loading-template').html();
  },
  
  updateTask: function(item){
    item.data({
      text: item.find('input[name=text]').first().val(),
      is_completed: item.find('input[name=is_completed]').first().is(':checked') ? 1 : 0,
      due_date: item.find('input[name=due_date]').first().val(),
      priority: item.find('button[name=priority].active').first().val()
    })
  },
  
  addTask: function(task, mode){
    
    var tmpl = (mode == 'edit') ? this.getTaskForm(task) : this.getTaskView(task);
    var oldMode = (mode == 'view') ? 'edit' : 'view';
    
    var item = this.tasks.find('li:data("id=' + task.id + '")').first();

    if(item.length > 0){
      
      item.html(tmpl)
        .removeClass(oldMode)
        .addClass(mode)
        .removeClass('completed');
      
    }else{
      
      item = $('<li></li>')
        .addClass('task')
        .addClass(mode)
        .data(task)
        .hide()
        .append(tmpl);
        
      this.tasks.append(item);
      
      item.slideDown('slow');
    }
    
    if(task.is_completed == 1){
      item.addClass('completed');
    }
  },
  
  loadEvents: function(){
    
    var self = this;
    
    $('.list-options').find('button').click(function(){
      self.getTasks({
        orderby: $(this).val()
      });
    });
    
    // Add new task
    $('#add-task').click(function(){
      
      var item = self.tasks.find('li:data("id=0")').first();
      
      if(item.length > 0){
        item.find('input[name=text]').focus();
        self.addNotification('error', 'Please complete the task added recently');
      }else{
        self.addTask({
          id: 0,
          text: '',
          is_completed: 0,
          due_date: '',
          priority: 1
        }, 'edit');
      }
    });
    
    $('#tasks').on('click', '.task.view', function(){
      self.addTask($(this).data(), 'edit');
    });
    
    $('#tasks').on('click', '.task.edit .save', function(){
      
      var item = $(this).parents('li');
      
      if(item.find('input[name=text]').first().val() == ''){
  
        self.addNotification('error', 'The task is empty!');
        item.find('input[name=text]').first().focus();
        
      }else{
        
        self.updateTask(item);

        item.find('form').fadeOut('slow', function(){
            $(this).html(self.getLoading()).fadeIn();
        });

        $.ajax({
          url: '/tasks/save',
          data: item.data(),
          type: 'POST'
        }).done(function (response, textStatus, jqXHR){
          if(response.error){
            self.addNotification('error', response.message);
            self.addTask(item.data(), 'view');
          }else{
            item.find('form').fadeOut('slow');
            item.data('id', response.task.id);
            self.addTask(response.task, 'view');
            self.addNotification('success', 'Task saved succesfully');
          }
        });
      }
    });
    
    $('#tasks').on('click', '.task.edit .cancel', function(){
      $(this).parents('li').slideUp('slow', function(){
        $(this).remove();
      });
    });
    
    $('#tasks').on('click', '.task.edit .delete', function(){
      
      var item = $(this).parents('li');

      item.find('form').fadeOut('slow', function(){
        $(this).html(self.getLoading()).fadeIn();
      });

      $.ajax({
        url: '/tasks/delete',
        data: item.data()
      }).done(function (response, textStatus, jqXHR){
        
        if(response.error){
          self.addTask(item.data());
          self.addNotification('error', response.message);
        }else{
          item.slideUp('slow', function(){
            $(this).remove();
          });
          self.addNotification('success', 'Task correctly removed!');
        }
      });      
    });
  },
  
  getOrder: function(){
    return $('.list-options').find('button.active').val();
  },

  getTasks: function(params){
    
    var self = this;
    
    this.tasks.find('li').slideUp('slow');
    
    var loading = $('<li></li>').addClass('task').html(self.getLoading()).hide();

    self.tasks.append(loading);

    loading.slideDown('slow');

    $.ajax({
      url: '/tasks/get',
      data: params,
      type: 'POST'
    }).done(function (response, textStatus, jqXHR){
      loading.slideUp('slow', function(){
        
        self.tasks.find('li').remove();
        
        if(response.error){
          self.addNotification('error', response.message);
        }else{
          $.each(response.tasks, function(key, task){
            self.addTask(task, 'view');
          });
        }
        
      });
    });
  }
};

$(document).ready(function(){
  todo.init();
});