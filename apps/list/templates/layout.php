<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php include_stylesheets() ?>
  </head>
  <body>
    <div class="wrap">
      <div class="navbar">
        <div class="navbar-inner">
          <a class="brand" href="#">
            <?php echo image_tag('toptal.png'); ?>
          </a>
        </div>
      </div>
      <div class="content">
        <?php echo $sf_content ?>
      </div>
    </div>
    <?php include_javascripts() ?>
  </body>
</html>
