<?php

class tasksActions extends sfActions
{
  private function callApi($service, $method, $params = array())
  {
    sleep(2);
    
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, sfConfig::get('app_api_url') . $service); 
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, sfConfig::get('app_api_user') . ':' . sfConfig::get('app_api_password'));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    if(!empty($params))
    {
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params)); 
    }
    
    $response = curl_exec($ch);
    
    curl_close($ch);
    
    return json_decode($response);
  }
  
  public function preExecute()
  {
    $this->getResponse()->setContentType('application/json');
  }

  public function executeGet(sfWebRequest $request)
  {
    $params = array(
      'orderby' => $request->getParameter('orderby'),
      'sort' => $request->getParameter('sort', 'asc'),
      'all' => $request->getParameter('all', 0)
    );
    
    $response = $this->callApi('/tasks?' . http_build_query($params), 'GET');
    
    return $this->renderText(json_encode($response));
  }
  
  public function executeSave(sfWebRequest $request)
  {
    $method = 'POST';
    $url = '/tasks';
    
    if($request->getParameter('id', 0) > 0)
    {
      $method = 'PUT';
      $url .= '/' . $request->getParameter('id');
    }
    
    $params = array(
      'text' => $request->getParameter('text'),
      'due_date' => $request->getParameter('due_date'),
      'priority' => $request->getParameter('priority'),
      'is_completed' => $request->getParameter('is_completed')
    );
    
    $response = $this->callApi($url, $method, $params);
    
    return $this->renderText(json_encode($response));
  }
  
  public function executeDelete(sfWebRequest $request)
  {
    $response = $this->callApi('/tasks/' . $request->getParameter('id', 0), 'DELETE');
    
    return $this->renderText(json_encode($response));
  }
}
