<script type="text/template" id="loading-template">
  <div class="loading">
    <?php echo image_tag('loading.gif'); ?> Loading
  </div>
</script>
<script type="text/template" id="notification-template">
  <div class="alert alert-<%= type %>">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <%= message %>
  </div>
</script>
<script type="text/template" id="task-view-template">
  <% if(priority == 1){ %>
  <i class="icon-ok"></i>
  <% } %>
  <% if(priority == 2){ %>
  <i class="icon-warning-sign"></i>
  <% } %>
  <% if(priority == 3){ %>
  <i class="icon-fire"></i>
  <% } %>
  <% if(due_date != '' && due_date != null){ %>
  <div class="due_date">[<%= due_date %>]</div>
  <% } %>
  <div class="text"><%= text %></div>
</script>
<script type="text/template" id="task-form-template">
  <form class="form-inline">
    <div class="is_completed">
      <input type="checkbox" name="is_completed" value="1" <% if(is_completed){ %>checked="checked"<% } %> />
    </div>
    <div class="text">
      <input type="text" name="text" value="<%= text %>" />
    </div>
    <div class="due_date">
      <input type="date" name="due_date" value="<%= due_date %>" />
    </div>
    <div class="priority">
      <div class="btn-group" data-toggle="buttons-radio">
        <button type="button" name="priority" value="1" class="btn btn-info <% if(priority == 1){ %>active<% } %>"><i class="icon-ok"></i> Low</button>
        <button type="button" name="priority" value="2" class="btn btn-info <% if(priority == 2){ %>active<% } %>"><i class="icon-warning-sign"></i> Medium</button>
        <button type="button" name="priority" value="3" class="btn btn-info <% if(priority == 3){ %>active<% } %>"><i class="icon-fire"></i> High</button>
      </div>
    </div>
    <div class="actions">
      <button type="button" class="btn btn-success save"><i class="icon-ok"></i> Save</button>
      <% if(id == 0){ %>
      <button type="button" class="btn btn-danger cancel"><i class="icon-trash"></i> Cancel</button>
      <% }else{ %>
      <button type="button" class="btn btn-danger delete"><i class="icon-trash"></i> Delete</button>
      <% } %>
    </div>
  </form>
</script>