<?php include_partial('templates'); ?>
<div class="notifications"></div>
<div class="list-options">
  <div class="btn-group" data-toggle="buttons-radio">
    <button type="button" name="orderby" value="due_date" class="btn btn-primary active"><i class="icon-calendar"></i> Due date</button>
    <button type="button" name="orderby" value="priority" class="btn btn-primary"><i class="icon-fire"></i> Priority</button>
  </div>
</div>
<ul id="tasks"></ul>
<div class="add-task" id="add-task">
  <i class="icon-plus"></i> Add task
</div>