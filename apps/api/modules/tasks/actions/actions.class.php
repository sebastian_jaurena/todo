<?php

class tasksActions extends sfActions
{
  public function preExecute()
  {
    $this->getResponse()->setContentType('application/json');

    $httpUser = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '';
    $httpPass = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';
    
    if($httpUser != '' && $httpPass != '')
    {
      $user = Doctrine::getTable('User')->findOneByUserAndPassword($httpUser, sha1($httpPass));

      if($user)
      {
        $this->userID = $user->getId();
      }
    }
    
    if(!$this->userID)
    {
      $this->getResponse()->setContent(json_encode(array('error' => 'Unauthorize access')));
      $this->getResponse()->setStatusCode(401);
      $this->getResponse()->sendHttpHeaders();
      $this->getResponse()->sendContent();
      
      throw new sfStopException();
    }
  }

  public function executeError(sfWebRequest $request)
  {
    $this->getResponse()->setStatusCode(404);
    $response = new stdClass();
    $response->error = 'Service not implemented';
    return $this->renderText(json_encode($response));
  }
  
  public function executeGet(sfWebRequest $request)
  {      
    $response = new stdClass();
    
    if($request->getParameter('id'))
    {
      $task = Doctrine::getTable('Task')->findOneByUserIdAndId($this->userID, $request->getParameter('id'));
      
      if(!$task)
      {
        $this->getResponse()->setStatusCode(404);
        $response->error = 'Task not found';
      }
      else
      {
        $response->task = array(
          'id' => $task->getId(),
          'text' => $task->getText(),
          'due_date' => $task->getDueDate(),
          'priority' => $task->getPriority(),
          'is_completed' => $task->getIsCompleted()
        );
      }
    }
    else
    {
      $all = $request->getParameter('all', 0);
      $orderby = $request->getParameter('orderby', 'id');
      $sort = $request->getParameter('sort', 'asc');
      
      $query = Doctrine::getTable('Task')
                ->createQuery()
                ->where('user_id = ?', $this->userID);

      if($all != 0)
      {
        $query->addWhere('is_completed = ?', 0);
      }

      if(!empty($orderby))
      {
        $query->orderBy($orderby . ' ' . $sort);
      }

      $tasks = $query->execute();
      
      foreach($tasks as $task)
      {
        $response->tasks[] = array(
          'id' => $task->getId(),
          'text' => $task->getText(),
          'due_date' => $task->getDueDate(),
          'priority' => $task->getPriority(),
          'is_completed' => $task->getIsCompleted()
        );
      }
    }
    
    return $this->renderText(json_encode($response));
  }
  
  public function executePost(sfWebRequest $request)
  {
    $response = new stdClass();
    
    $response->error = false;
    
    if(!$request->getParameter('text'))
    {
      $this->getResponse()->setStatusCode(500);
      $this->error = true;
      $response->message = 'Empty task';
    }
    else
    {
      $task = new Task();
      
      $task->setUserId($this->userID);
      $task->setText($request->getParameter('text'));
      
      if($request->getParameter('due_date'))
      {
        $task->setDueDate($request->getParameter('due_date'));
      }
      
      if($request->getParameter('priority'))
      {
        $task->setPriority($request->getParameter('priority'));
      }
      
      if($request->getParameter('is_completed'))
      {
        $task->setIsCompleted($request->getParameter('is_completed'));
      }
      
      $task->save();
      
      $response->task = $task->getDump();
    }
    
    return $this->renderText(json_encode($response));
  }
  
  public function executePut(sfWebRequest $request)
  {
    $response = new stdClass();
    
    $response->error = false;
    
    if(!$request->getParameter('id'))
    {
      $this->getResponse()->setStatusCode(400);
      
      $response->error = true;
      $response->message = 'Service not exist';
    }
    else
    {
      $task = Doctrine::getTable('Task')->findOneByUserIdAndId($this->userID, $request->getParameter('id'));

      if(!$task)
      {
        $this->getResponse()->setStatusCode(404);
        
        $response->error = true;
        $response->message = 'Task not found';
      }
      else
      { 
        $task->setText($request->getParameter('text'));
        $task->setDueDate($request->getParameter('due_date'));
        $task->setPriority($request->getParameter('priority'));
        $task->setIsCompleted($request->getParameter('is_completed'));
        
        $task->save();
        
        $response->task = $task->getDump();
      }
    }
    
    return $this->renderText(json_encode($response));
  }
  
  public function executeDelete(sfWebRequest $request)
  {
    $response = new stdClass();
    
    $response->error = false;
    
    if(!$request->getParameter('id'))
    {
      $this->getResponse()->setStatusCode(400);
      
      $response->error = true;
      $response->message = 'Service not exist';
    }
    else
    {
      $task = Doctrine::getTable('Task')->findOneByUserIdAndId($this->userID, $request->getParameter('id'));
      
      if(!$task)
      {
        $this->getResponse()->setStatusCode(404);
        
        $response->error = true;
        $response->message = 'Task not found';
      }
      else
      {
        $task->delete();
      }
    }
    
    return $this->renderText(json_encode($response));
  }
}
